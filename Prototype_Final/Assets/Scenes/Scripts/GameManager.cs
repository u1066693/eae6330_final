using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public AudioSource bgm;

    public static bool isVictory = false;

    public GameObject victoryUI;

    // Start is called before the first frame update
    void Start()
    {
        bgm.Play();
    }

    // Update is called once per frame
    void Update()
    {

        if (isVictory)
            victoryUI.SetActive(true);
    }
}
