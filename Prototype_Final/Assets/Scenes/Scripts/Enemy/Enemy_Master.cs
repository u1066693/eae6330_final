using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Master : MonoBehaviour
{
    public NavMeshAgent navAgent;
    public Transform player;
    public LayerMask isGround, isPlayer;

    // Patrolling
    public Vector3 walkPoint;
    private bool walkPointIsSet;
    public float walkPointRange;

    // Attack
    public float attackInterval;
    protected bool attacked;

    // States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public float health;

    public Animator animator;

    protected bool isDead = false;

    protected AudioSource audioManager;
    public AudioClip attackingSound;
    public AudioClip dieSound;

    public GameObject bullet;
    protected void Awake()
    {
        audioManager = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        player = GameObject.Find("Player-HDRP").transform;
        navAgent = GetComponent<NavMeshAgent>();
    }

    private void Patrolling()
    {
        if (!walkPointIsSet) SearchWalkPoint();

        if (walkPointIsSet)
            navAgent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
            walkPointIsSet = false;
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);
        walkPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, isGround))
            walkPointIsSet = true; 
    }

    private void ChasePlayer()
    {
        navAgent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        navAgent.SetDestination(transform.position);
        transform.LookAt(player);

        if (!attacked)
        {
            // Attack code here
            //Debug.LogWarning("Attacked!");
            animator.SetBool("isAttacking", true);

            audioManager.PlayOneShot(attackingSound);
            attacked = true;
            Invoke(nameof(ResetAttack), attackInterval);
        }
    }

    private void ResetAttack()
    {
        animator.SetBool("isAttacking", false);
        attacked = false;
    }

    private void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Invoke(nameof(DestroyEnemy), 0.5f);
        }
    }

    private void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        //if (audioManager.clip != null)
        //    audioManager.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            playerInSightRange = Physics.CheckSphere(transform.position, sightRange, isPlayer);
            playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, isPlayer);

            if (!playerInSightRange && !playerInAttackRange) Patrolling();
            if (playerInSightRange && !playerInAttackRange) ChasePlayer();
            if (playerInSightRange && playerInAttackRange) AttackPlayer();
        }


    }

    protected void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }

    private void OnCollisionEnter(Collision collision)
    {

    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 12)
        {
            audioManager.PlayOneShot(dieSound);       
            animator.SetBool("isDead", true);
            isDead = true;
            gameObject.GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
