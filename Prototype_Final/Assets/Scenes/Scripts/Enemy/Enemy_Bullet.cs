
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Bullet : MonoBehaviour
{
    public AudioSource audioManager;
    public AudioClip bulletBlocked;

    // Start is called before the first frame update
    void Start()
    {
        audioManager = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 12)
        {
            audioManager.PlayOneShot(bulletBlocked, 10.0f);
            Destroy(gameObject);
        }
    }
}
