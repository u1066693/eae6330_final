using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Ranged : Enemy_Master
{

    public GameObject bullectSpawnLocation;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        if (!isDead)
        {
            playerInSightRange = Physics.CheckSphere(transform.position, sightRange, isPlayer);
            playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, isPlayer);

            if (playerInSightRange && !playerInAttackRange) ChasePlayer();
            if (playerInSightRange && playerInAttackRange) AttackPlayer();
        }

    }

    void ChasePlayer()
    {
        transform.LookAt(player);
        animator.SetBool("seePlayer", true);

    }

    private void AttackPlayer()
    {
        //navAgent.SetDestination(transform.position);
        transform.LookAt(player);

        
        if (!attacked)
        {
            Debug.LogError("Ranged Attack!");
            // Attack code here
            //Debug.LogWarning("Attacked!");
            animator.SetBool("isAttacking", true);

            GameObject firedbullet = Instantiate(bullet,
                bullectSpawnLocation.transform.position,
                Quaternion.identity) as GameObject;

            firedbullet.transform.LookAt(player);
            firedbullet.GetComponent<Rigidbody>().AddForce(firedbullet.transform.forward * 200);

            audioManager.PlayOneShot(attackingSound, 10.0f);
            attacked = true;
            Invoke(nameof(ResetAttack), attackInterval);
        }
    }

    private void ResetAttack()
    {
        animator.SetBool("isAttacking", false);
        attacked = false;
    }

}
