using UnityEngine;
using System.Collections;

namespace XEntity
{
    //This is the scriptable object that holds the data template for the items.
    [CreateAssetMenu(fileName = "New Item", menuName = "XEntity/Item")]
    public class Item : ScriptableObject
    {

        string path = "Player-HDRP/RFA_Model/SK_RFA/root/ik_hand_root/ik_hand_gun/ik_hand_r";

        #region Essential
        public ItemType type;
        public string itemName;
        public int itemPerSlot;
        public Sprite icon;
        public GameObject prefab;

        public string EquipObjectName;
        #endregion

        public void Equip()
        {
            Debug.Log(GameObject.Find(path + '/' + EquipObjectName));
            Debug.Log(path + '/' + EquipObjectName);

            GameObject.Find(path + '/' + EquipObjectName).SetActive(true);

            Retro.ThirdPersonCharacter.PlayerInput.abilityGot = true;

            

            //GameObject.Find("Player-HDRP").transform.Find("")

            // FindChild(EquipObjectName).gameObject.SetActive(true);
        }

        public void Consume()
        {
            GameManager.isVictory = true;
        }

        void HideAbilityUI()
        {

        }

        IEnumerator Wait()
        {
            yield return new WaitForSeconds(3);
            HideAbilityUI();
        }


    }
}
