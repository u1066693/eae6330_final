using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Retro.ThirdPersonCharacter
{
    public class PlayerInput : MonoBehaviour
    {
        private bool _attackInput;
        private bool _specialAttackInput;
        private Vector2 _movementInput;
        private bool _jumpInput;
        private bool _changeCameraModeInput;

        

        public bool AttackInput {get => _attackInput;}
        public bool SpecialAttackInput {get => _specialAttackInput;}
        public Vector2 MovementInput {get => _movementInput;}
        public bool JumpInput { get => _jumpInput; }
        public bool ChangeCameraModeInput {get => _changeCameraModeInput;}

        [Header("Time Ability")]
        public Image abilityBar;
        private bool canUseAbility = true;
        private const float abilityCost = 0.01f;
        private const float abilityRecover = 0.02f;

        public GameObject directionalLight;
        private const float lightOriginIntensity = 6000.0f;
        private const float lightCost = 1000f;


        public static bool abilityGot = false;
        private void Update()
        {
            _attackInput = Input.GetMouseButtonDown(0);
            _specialAttackInput = Input.GetMouseButtonDown(1);

            //_movementInput.Set(Input.GetAxis("Horizontal"),Input.GetAxis("Vertical"));
            _movementInput.Set(Input.GetAxis("Horizontal"),0);
            _jumpInput = Input.GetButton("Jump");
            _changeCameraModeInput = Input.GetKeyDown(KeyCode.F);


            // Slow down time
            if (Input.GetKey(KeyCode.LeftShift) && canUseAbility && abilityGot)
            {
                if (Time.timeScale == 1.0f)
                    Time.timeScale = 0.2f;

                CostAbility();
            }
            else
            {
                RecoverAbility();
            }

            if (Input.GetKeyUp(KeyCode.LeftShift) || !canUseAbility)
            {
                
                canUseAbility = true;
                if (Time.timeScale == 0.2f)
                    Time.timeScale = 1.0f;                
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(0);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
        }

        private void CostAbility()
        {
            abilityBar.fillAmount -= abilityCost;
            if (abilityBar.fillAmount <= 0)
            {
                canUseAbility = false;
            }
            
            
            if (directionalLight.GetComponent<Light>().intensity > 0)
            {
                directionalLight.GetComponent<Light>().intensity -= lightCost;
            }
        }

        private void RecoverAbility()
        {
            
            if (abilityBar.fillAmount < 1)
            {
                abilityBar.fillAmount += abilityRecover;
            }

            if (directionalLight.GetComponent<Light>().intensity < lightOriginIntensity)
            {
                directionalLight.GetComponent<Light>().intensity += lightCost;
            }
        }

    }
}