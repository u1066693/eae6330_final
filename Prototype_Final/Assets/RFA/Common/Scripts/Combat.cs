﻿using UnityEngine;
using NaughtyCharacter;
using UnityEngine.Experimental.VFX;
using UnityEngine.VFX;


namespace Retro.ThirdPersonCharacter
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(Animator))]
    public class Combat : MonoBehaviour
    {
        private const string attackTriggerName = "Attack";
        private const string specialAttackTriggerName = "Ability";

        private Animator _animator;
        private PlayerInput _playerInput;

        public bool isDead = false;
        public bool AttackInProgress {get; private set;} = false;

        public GameObject attackEffect;

        public AudioClip attackSound;

        private AudioSource soundManager;
        private void Start()
        {
            soundManager = gameObject.GetComponent<AudioSource>();
            attackEffect.GetComponent<CapsuleCollider>().enabled = false;
            _animator = GetComponent<Animator>();
            _playerInput = GetComponent<PlayerInput>();
        }

        private void Update()
        {
            if(_playerInput.AttackInput && !AttackInProgress && !isDead)
            {
                Attack();
            }
            else if (_playerInput.SpecialAttackInput && !AttackInProgress)
            {
                SpecialAttack();
            }
        }

        private void SetAttackStart()
        {
            AttackInProgress = true;
        }

        private void SetAttackEnd()
        {
            AttackInProgress = false;
        }

        private void Attack()
        {
            _animator.SetTrigger(attackTriggerName);
            soundManager.PlayOneShot(attackSound);
            attackEffect.GetComponent<VisualEffect>().Play();
            attackEffect.GetComponent<CapsuleCollider>().enabled = true;
            Invoke(nameof(DisableAttackCollision), 0.1f);

            //Debug.LogError("Player Attack");

        }

        private void SpecialAttack()
        {
            _animator.SetTrigger(specialAttackTriggerName);
        }

        private void DisableAttackCollision()
        {
            attackEffect.GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}