﻿using UnityEngine;

namespace Retro.ThirdPersonCharacter
{
    [RequireComponent(typeof(PlayerInput))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Combat))]
    [RequireComponent(typeof(CharacterController))]
    public class Movement : MonoBehaviour
    {
        private Animator _animator;
        private PlayerInput _playerInput;
        private Combat _combat;
        private CharacterController _characterController;

        private Vector2 lastMovementInput;
        private Vector3 moveDirection = Vector3.zero;

        public float gravity = 10;
        public float jumpSpeed = 4; 

        public float MaxSpeed = 10;
        private float DecelerationOnStop = 0.00f;

        private bool isDead = false;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _playerInput = GetComponent<PlayerInput>();
            _combat = GetComponent<Combat>();
            _characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
            if (_animator == null) return;

            //if(_combat.AttackInProgress)
            //{
            //    StopMovementOnAttack();
            //}
            //else
            //{
            if (!isDead)
               Move();
            //}

        }
        private void Move()
        {
            var x = _playerInput.MovementInput.x;
            var y = _playerInput.MovementInput.y;

            if(x < 0)
            {
                //gameObject.transform.Find("RFA_Model").gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
                gameObject.transform.Find("RFA_Model").transform.eulerAngles = new Vector3(0.0f, 3.14f, 0.0f);
                gameObject.transform.Find("Sword Slash Yellow").transform.eulerAngles = new Vector3(0.0f, 0.0f, 39f);
            }
            else if(x > 0)
            {
                gameObject.transform.Find("RFA_Model").gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
                gameObject.transform.Find("Sword Slash Yellow").transform.eulerAngles = new Vector3(0.0f, 180f, -51f);
            }

            bool grounded = _characterController.isGrounded;

            if (grounded)
            {
                moveDirection = new Vector3(0, 0, x);
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= MaxSpeed;
                if (_playerInput.JumpInput)
                    moveDirection.y = jumpSpeed;
            }

            moveDirection.y -= gravity * Time.deltaTime;
            _characterController.Move(moveDirection * Time.deltaTime);
            gameObject.transform.position.Set(-5.0f, gameObject.transform.position.y, gameObject.transform.position.z);

            //_animator.SetFloat("InputX", x);
            _animator.SetFloat("InputY", x);
            _animator.SetBool("IsInAir", !grounded);
        }

        private void StopMovementOnAttack()
        {
            var temp = lastMovementInput;
            temp.x -= DecelerationOnStop;
            temp.y -= DecelerationOnStop;
            lastMovementInput = temp;

            _animator.SetFloat("InputX", lastMovementInput.x);
            _animator.SetFloat("InputY", lastMovementInput.y);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.layer == 13)
            {
                _animator.SetBool("isDead", true);
                isDead = true;
                _combat.isDead = true;


                //gameObject.GetComponent<CapsuleCollider>().enabled = false;
            }
        }
    }
}